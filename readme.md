## Projet BTS SIO : Générateur de paysages virtuels 

Le projet Virtual-landspace constistait à développer une petite application grâce à l'API 2D javascript, et cela, dans une approche objet et événementielle. L'application aura pour but de permettre a l'utilisateur de pouvoir générer aléatoirement des paysages virtuel et de les sauvegarder.

##### Délivrée par un serveur HTTP

Attention, l'application doit être placée derrière en serveur HTTP, et donc accessible à un utilisateur en réponse à une requête de type http:// (et non en protocole file://)

### Squelette de l'application

---
#### structure des dossiers

<pre>        .
        ├── css
        │   ├── lavalamp.png
        │   ├── layout.css
        │   ├── menu_bg.png
        │   ├── menu.css
        │   └── menu_line.png
        ├── js
        │   ├── main.js
        │   └── modules
        │       ├── AbstractForm.js
        │       ├── Building.js
        |       ├── Window.js
        │       └── Door.js
        └── index.html

</pre>

* `index.html` : le point d'entrée de l'interprétation par un navigateur. Hormis les inclusion `css`.

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8"/>
  <!--<meta name="viewport" content="width=device-width, initial-scale=1">-->
  <title>Virtual Landscape Project</title>
  <link href="css/layout.css" rel="stylesheet" type="text/css"/>
  <link href="css/menu.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<header>
  <ul id="nav">
    <li><a href="#" id="all">&nbsp;&nbsp;All&nbsp;&nbsp;</a></li>
    <li><a href="" id="saveImage">Download</a></li>
    <div id="lavalamp"></div>
  </ul>
</header>
<div class="container">
  <canvas id="sceneryCanvas" style="width: 100%"></canvas>
</div>
<script type="module" src="js/main.js"></script>
</body>
</html>
```

* `css` : l'application utilise le template : http://www.script-tutorials.com/pure-css3-lavalamp-menu/
* `main.js` : déclare utiliser des modules (des classes `js`) et définit des fonctions permettant d'afficher le paysage ainsi que de pouvoir le télécharger : 

 `main.js` 
```javascript 
/**
 * construit les différentes formes du paysage, en appelant la méthode statique
 * buildForms de chacune des classes
 *
 * @return {Object[]}
 */
function buildForms() {
  let forms = Building.buildForms()
  return forms
}

/**
 * Le paysage est dessiné une fois la page chargée.
 */
addEventListener('DOMContentLoaded', event => {
  drawForms(buildForms())
});

const drawAllForm = document.getElementById("all")
drawAllForm.addEventListener('click', event => {
  drawForms(buildForms())
});


/**
 * Save image
 * @param idCanvas
 * @param nomFichier
 */
function saveImage(idCanvas, nomFichier) {
  // nom du fichier pour l'enregistrement
  const nomFile = nomFichier
  // récup. de l'élément <canvas>
  const canvas = document.getElementById(idCanvas)
  let dataImage
  // pour IE et Edge c'est simple !!!
  if (canvas.msToBlob) {
    // crée un objet blob contenant le dessin du canvas
    dataImage = canvas.msToBlob();
    // affiche l'invite d'enregistrement
    window.navigator.msSaveBlob(dataImage, nomFile)
  }
  else {
    // création d'un lien HTML5 download
    const lien = document.createElement("A")
    // récup. des data de l'image
    dataImage = canvas.toDataURL("image/png")
    // affectation d'un nom à l'image
    lien.download = nomFile
    // modifie le type de données
    dataImage = dataImage.replace("image/png", "image/png")
    // affectation de l'adresse
    lien.href = dataImage
    // ajout de l'élément
    document.body.appendChild(lien);
    // simulation du click
    lien.click()
    // suppression de l'élément devenu inutile
    document.body.removeChild(lien)
  }
}

const saveImg = document.getElementById("saveImage")
saveImg.addEventListener('click', event => {
  saveImage('sceneryCanvas','buildings')
});
```

Le dossier modules : il contient le code source de classes javascript. 

Les formes seront représentées par des classes.

![](readme-img/diagram.PNG)

![](readme-img/diagramModules.PNG)

#### Création d'un building : 

`Building.js` 
![](readme-img/building.PNG)

![](readme-img/fortab.PNG)

#### Quelques captures d'écran : 

![](readme-img/Capture1.PNG)

![](readme-img/Capture2.PNG)

![](readme-img/Capture3.PNG)


#### Principales Ressources utilisées :

Dessiner dans un canvas : https://www.w3schools.com/graphics/canvas_drawing.asp">https://www.w3schools.com/graphics/canvas_drawing.asp

Pouvoir télécharger l'image d'un canvas en js : https://nosmoking.developpez.com/demos/js/canvas-download.html">https://nosmoking.developpez.com/demos/js/canvas-download.html

