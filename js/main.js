import { Building } from ".//modules/Building.js";

var cwPrev = null
var chPrev = null

function clearCanvas() {
  const c = document.getElementById('sceneryCanvas')
  const ctx = c.getContext("2d");

  if (cwPrev) {
    ctx.clearRect(0, 0, cwPrev, chPrev)
  }
  const cw = c.width = window.innerWidth;
  const ch = c.height = window.innerHeight - 80;

  console.log("window.innerHeight : " + window.innerHeight);

  cwPrev = cw
  chPrev = ch
}

/**
 * Dessine tous les objets dans le canvas
 * @param forms tableau d'objets de type AbstractForm
 * @private
 */
function drawForms(forms) {
  const c = document.getElementById('sceneryCanvas')
  const ctx = c.getContext("2d");

  clearCanvas()
  console.log("forms :" + JSON.stringify(forms))

  // draw all forms by looping over them
  forms.forEach(form => form.draw(ctx))

}

/**
 * construit les différentes formes du paysage, en appelant la méthode statique
 * buildForms de chacune des classes
 *
 * @return {Object[]}
 */
function buildForms() {
  let forms = Building.buildForms()
  return forms
}

/**
 * Le paysage est dessiné une fois la page chargée.
 */
addEventListener('DOMContentLoaded', event => {
  drawForms(buildForms())
});

const drawAllForm = document.getElementById("all")
drawAllForm.addEventListener('click', event => {
  drawForms(buildForms())
});


/**
 * Save image
 * @param idCanvas
 * @param nomFichier
 */
function saveImage(idCanvas, nomFichier) {
  // nom du fichier pour l'enregistrement
  const nomFile = nomFichier
  // récup. de l'élément <canvas>
  const canvas = document.getElementById(idCanvas)
  let dataImage
  // pour IE et Edge c'est simple !!!
  if (canvas.msToBlob) {
    // crée un objet blob contenant le dessin du canvas
    dataImage = canvas.msToBlob();
    // affiche l'invite d'enregistrement
    window.navigator.msSaveBlob(dataImage, nomFile)
  }
  else {
    // création d'un lien HTML5 download
    const lien = document.createElement("A")
    // récup. des data de l'image
    dataImage = canvas.toDataURL("image/png")
    // affectation d'un nom à l'image
    lien.download = nomFile
    // modifie le type de données
    dataImage = dataImage.replace("image/png", "image/png")
    // affectation de l'adresse
    lien.href = dataImage
    // ajout de l'élément
    document.body.appendChild(lien);
    // simulation du click
    lien.click()
    // suppression de l'élément devenu inutile
    document.body.removeChild(lien)
  }
}

const saveImg = document.getElementById("saveImage")
saveImg.addEventListener('click', event => {
  saveImage('sceneryCanvas','buildings')
});