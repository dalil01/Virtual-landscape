import { AbstractForm } from './AbstractForm.js';

/**
 *  Cette classe permet de dessiner une fenêtre pour les bâtiments (classe Building).
 */
class Window extends AbstractForm {
    constructor(
        x = 0,
        y = 0,
        width = 0,
        height = 0,
        fillColor = '',
        strokeColor = '',
        strokeWidth = 2,
        pesanteur = false,
    ) {
        super(x, y, width, height, fillColor, strokeColor, strokeWidth, pesanteur)
    }

    /**
     * Dessine la forme spécifique à cette classe
     * @param ctx contexte 2D du canvas
     */
    draw(ctx) {
        ctx.save()
        ctx.beginPath()
        ctx.strokeStyle = this.strokeColor
        ctx.lineWidth = this.strokeWidth

        const MAX_HEAD = 80
        let new_y = (this.pesanteur) ? window.innerHeight - this.height - MAX_HEAD : this.y
        ctx.rect(this.x, new_y, this.width, this.height)

        ctx.fillStyle = this.fillColor

        ctx.closePath()
        ctx.fill()
        ctx.stroke()
        ctx.restore()
    }
}
export { Window }