import {AbstractForm} from './AbstractForm.js';
import {Door} from "./Door.js";
import {Window} from "./Window.js";

/**
 * La classe Building utilise les classes Door et Window afin de construit un paysage dynamique
 * Chaque méthode (form) représente un building.
 */
class Building extends AbstractForm {
    // you create new Rectangles by calling this as a function
    // these are the arguments you pass in
    // add default values to avoid errors on empty arguments
    constructor(
        x = 0,
        y = 0,
        width = 0,
        height = 0,
        fillColor = '',
        strokeColor = '',
        strokeWidth = 2,
        pesanteur = false,
        doorTab = [],
        windowTab = []
    ) {
        super(x, y, width, height, fillColor, strokeColor, strokeWidth, pesanteur)
        this.doorTab = doorTab
        this.windowTab = windowTab
    }

    /**
     * Dessine la forme spécifique à cette classe
     * @param ctx contexte 2D du canvas
     */
    draw(ctx) {
        ctx.save()
        ctx.beginPath()
        ctx.strokeStyle = this.strokeColor
        ctx.lineWidth = this.strokeWidth

        const MAX_HEAD = 80
        let new_y = (this.pesanteur) ? window.innerHeight - this.height - MAX_HEAD : this.y
        ctx.rect(this.x, new_y, this.width, this.height)

        ctx.fillStyle = this.fillColor

        ctx.closePath()
        ctx.fill()
        ctx.stroke()
        ctx.restore()

        // Door
        if (this.doorTab.length > 0) {
            for (let i = 0; i < this.doorTab.length; i++) {
                this.doorTab[i].draw(ctx)
            }
        }

        // Window
        if (this.windowTab.length > 0) {
            for (let i = 0; i < this.windowTab.length; i++) {
                this.windowTab[i].draw(ctx)
            }
        }
    }

    /**
     * Draw different buildings
     * @returns {Building}
     */
    form1() {
        // For Window - 1
        const window1Y =  window.innerHeight - this.height - 455
        const window1ColorTab = ["navy","lime", "indigo"];
        let i = Math.random() * window1ColorTab.length;

        // For Window - 2
        const window2Y =  window.innerHeight - this.height - 455
        const window2ColorTab = ["gold","magenta", "pink"];
        let j = Math.random() * window2ColorTab.length;

        // For Window - 3
        const window3Y =  window.innerHeight - this.height - 455
        const window3ColorTab = ["red","aqua", "maroon"];
        let k = Math.random() * window3ColorTab.length;

        const form = new Building(23, 0, 95, 380, "#000", "#000", 0, true,
                [new Door(50, 0, 40, 50, "#2F2F2F", "#000", 3, true)],
                [new Window(29, window1Y, 18, 368, window1ColorTab[parseInt(i)], "#fff", 4, false),
                new Window(61, window2Y, 18, 321, window2ColorTab[parseInt(j)], "#fff", 4, false),
                new Window(94, window3Y, 18, 368, window3ColorTab[parseInt(k)], "#fff", 4, false)])

        return form

    }

    form2() {
        const form = new Building(90, 0, 150, 160, "#2C2C2C", "#000", 2, true,
            [new Door(146, 0, 20, 25, "#878787", "#000", 1, true)],
            [])

        // For Window - All
        const window1Y =  window.innerHeight - this.height - 216
        const window2Y =  window.innerHeight - this.height - 162
        const windowsColorTab = ["red","blue","green"];
        let i = Math.random() * windowsColorTab.length;

        let wX = 110
        for(let k = 0; k < 5; k++){
            form.windowTab.push(
                new Window(wX, window1Y, 15, 32, "#FFF", "#000", 2, false),
                new Window(wX, window2Y, 15, 32, windowsColorTab[parseInt(i)], "#000", 2, false)
            )
            wX += 25
        }
        return form
    }

    form3() {
        const posY = window.innerHeight - this.height - 365
        const form = new Building(170, posY, 110, 270, "#DCDCDC", "#000", 1, false,
            [],
            [])

        // For Window - All
        let windowY =  window.innerHeight - this.height - 365
        const windowsColorTab = ["yellow","pink","chocolate"];
        let i = Math.random() * windowsColorTab.length;

        let wX = 172
        let c = 1
        for(let j = 0; j < 45 ; j++){
            form.windowTab.push(
                new Window(wX, windowY, 16, 16,  windowsColorTab[parseInt(i)], "#000", 2, false)
            )
            if(c == 6){
                windowY += 17
                wX = 154
                c = 0
            }
            wX += 18
            c++
        }
        return form
    }

    form4() {
        const form = new Building(215, 0, 135, 212, "#FFF", "#000", 2, true,
            [],
            [])

        // For Window - All
        const windowsColorTab = ["green"];
        let i = Math.random() * windowsColorTab.length;

        let window1Y =  window.innerHeight - this.height - 290
        for(let j = 0; j < 8; j++){
            form.windowTab.push(
                new Window(215, window1Y, 135, 14,  windowsColorTab[parseInt(i)], "#000", 2, false)
            )
            window1Y += 28
        }

        let window2X = 231
        for(let k = 0; k < 4; k++){
            form.windowTab.push(
                new Window(window2X, 0, 14, 212,  windowsColorTab[parseInt(i)], "#000", 2, true)
            )
            window2X += 30
        }
        return form
    }

    form5() {
        // For Door
        const doorPosY = window.innerHeight - this.height - 145

        const posY = window.innerHeight - this.height - 365
        const form = new Building(300, posY, 105, 270, "#DCDCDC", "#000", 1, false,
            [new Door(350, doorPosY, 40, 49, "#000", "#000", 1, false)],
            [])

        // For Window - All
        let windowY =  window.innerHeight - this.height - 365
        const windowsColorTab = ["yellow","pink","chocolate"];
        let i = Math.random() * windowsColorTab.length;

        let wX = 300
        let c = 1
        for(let j = 0; j < 80; j++){
            form.windowTab.push(
                new Window(wX, windowY, 16, 16,  windowsColorTab[parseInt(i)], "#000", 2, false)
            )
            if(c == 6){
                windowY += 17
                wX = 282
                c = 0
            }
            wX += 18
            c++
        }
        return form
    }

    form6() {
        const form = new Building(390, 0, 25, 120, "#585858", "#000", 2, true)
        return form
    }

    form7() {
        // For Window - 1
        const window1Y =  window.innerHeight - this.height - 235
        const window1ColorTab = ["red"];
        let i = Math.random() * window1ColorTab.length;

        // For Window - 2
        const window2Y =  window.innerHeight - this.height - 220
        const window2ColorTab = ["#1B1A1A"];
        let j = Math.random() * window1ColorTab.length;

        const form = new Building(415, 0, 30, 175, "#FFF", "#000", 2, true,
            [],
            [new Window(432, window1Y, 30, 90, window1ColorTab[parseInt(i)], "#000", 2, false),
                new Window(440, window2Y, 20, 60, window2ColorTab[parseInt(i)], "#000", 2, false)])
        return form
    }

    form8() {
        const form = new Building(445, 0, 120, 280, "#D1D1D1", "#000", 2, true,
            [new Door(490, 0, 30, 35, "#000", "#000", 1, true)],
            [])

        // For Window - All
        let windowY =  window.innerHeight - this.height - 351
        let wX = 455
        let c = 1
        for(let j = 0; j < 78 ; j++){
            form.windowTab.push(
                new Window(wX, windowY, 9, 10,  "transparent", "#000", 1, false)
            )
            if(c == 6){
                windowY += 18
                wX = 437
                c = 0
            }
            wX += 18
            c++
        }

        return form
    }

    form9() {
        const form = new Building(540, 0, 110, 155, "#D1D1D1", "#000", 1, true,
            [new Door(575, 0, 53, 40, "#2C2C2C", "#000", 1, true),
                new Door(584, 0, 35, 30, "#D1D1D1", "#000", 1, true),
                new Door(591, 0, 20, 21, "#000", "#000", 2, true)])

        // For Window - 1
        const window1Y =  window.innerHeight - this.height - 225
        let wX = 548
        for(let k = 0; k < 7; k++){
            form.windowTab.push(
                new Window(wX, window1Y, 10, 22, "transparent", "#000", 1, false),
            )
            wX += 18
        }

        // For Window - 2 - 5
        let window2Y =  window.innerHeight - this.height - 193
        let w2X = 548
        let c = 1
        for(let j = 0; j < 12 ; j++){
            form.windowTab.push(
                new Window(w2X, window2Y, 10, 10,  "transparent", "#000", 1, false)
            )
            if(c == 6){
                window2Y += 55
                w2X = 530
                c = 0
            }
            w2X += 18
            c++
        }

        // For Window - 3 - 4
        const windowsColorTab = ["transparent","#878787"];
        let window3Y =  window.innerHeight - this.height - 174
        let w3X = 548
        let co = 1
        for(let j = 0; j < 6 ; j++){
            let i = Math.random() * windowsColorTab.length;
            form.windowTab.push(
                new Window(w3X, window3Y, 30, 10,   windowsColorTab[parseInt(i)], "#000", 1, false)
            )
            if(co == 3){
                window3Y += 18
                w3X = 510
                co = 0
            }
            w3X += 38
            co++
        }
        return form
    }

    form10() {
        const form = new Building(648, 0, 128, 190, "#1B1A1A", "#000", 2, true,
            [new Door(687, 0, 50, 25, "#878787", "#000", 1, true),
                new Door(687, 0, 40, 17, "#1B1A1A", "#000", 2, true)],
            [])

        // For Window - All
        let windowY =  window.innerHeight - this.height - 255
        let wX = 659
        let c = 1
        for(let j = 0; j < 56 ; j++){
            form.windowTab.push(
                new Window(wX, windowY, 9, 9,  "#FFF", "#000", 1, false)
            )
            if(c == 7){
                windowY += 18
                wX = 643
                c = 0
            }
            wX += 16
            c++
        }
        return form
    }

    form11() {
        const form = new Building(670, 0, 127, 310, "#BABABA", "#000", 1, true,
            [],
            [])

        // For Window - All
        let windowY =  window.innerHeight - this.height - 380
        let wX = 678
        let c = 1
        for(let j = 0; j < 70 ; j++){
            form.windowTab.push(
                new Window(wX, windowY, 10, 25,  "#FAFAFA", "#000", 1, false)
            )
            if(c == 8){
                windowY += 33
                wX = 662
                c = 0
            }
            wX += 16
            c++
        }
        return form
    }

    form12() {
        const form = new Building(796, 0, 62, 360, "#878787", "#000", 2, true
        ,[],
            [])

        // For Window - All
        const windowColorTab = ["green","yellow","#DCDCDC","red","#2F2F2F","violet","blue"];
        let windowY =  window.innerHeight - this.height - 438
        let wX = 797
        let c = 1
        for(let j = 0; j < 24 ; j++){
            let k = Math.random() * windowColorTab.length;
            form.windowTab.push(
                new Window(wX, windowY, 24, 47,  windowColorTab[parseInt(k)], "transparent", 0, false)
            )
            if(c == 3){
                windowY += 47
                wX = 779
                c = 0
            }
            wX += 18
            c++
        }

        return form
    }

    form13() {
        const form = new Building(859, 0, 30, 135, "#585858", "#000", 0, true,
            [new Door(880, 0, 20, 25, "#000", "#000", 1, true)],
            [])

        // For Window - All
        let windowY =  window.innerHeight - this.height - 204
        let wX = 870
        let c = 1
        for(let j = 0; j < 6 ; j++){
            form.windowTab.push(
                new Window(wX, windowY, 20, 6,  "#FAFAFA", "#FFF", 0, false)
            )
            if(c == 1){
                windowY += 16
                wX = 853
                c = 0
            }
            wX += 18
            c++
        }

        return form
    }

    form14() {
        const form = new Building(890, 0, 80, 195, "#DCDCDC", "#000", 2, true,
            [new Door(920, 0, 25, 27, "#000", "#000", 1, true),
                new Door(920, 0, 18, 20, "#878787", "#000", 1, true)],
            [])

        // For Window - All
        const windowsColorTab = ["white","yellow"];
        let windowY =  window.innerHeight - this.height - 266
        let wX = 899
        let c = 1
        for(let j = 0; j < 40 ; j++){
            let i = Math.random() * windowsColorTab.length;
            form.windowTab.push(
                new Window(wX, windowY, 8, 8,  windowsColorTab[parseInt(i)], "#000", 1, false)
            )
            if(c == 4){
                windowY += 16
                wX = 881
                c = 0
            }
            wX += 18
            c++
        }

        return form
    }

    form15() {
        const form = new Building(970, 0, 137, 275, "#2C2C2C", "#000", 1, true,
            [new Door(1012, 0, 43, 42, "#000", "#DCDCDC", 1, true),
                new Door(1023, 0, 21, 30, "#878787", "#DCDCDC", 1, true)],
            [])

        // For Window - All
        const windowsColorTab = ["#DCDCDC","orange"];
        let windowY =  window.innerHeight - this.height - 345
        let wX = 978
        let c = 1
        for(let j = 0; j < 96 ; j++){
            let i = Math.random() * windowsColorTab.length;
            form.windowTab.push(
                new Window(wX, windowY, 9, 9,  windowsColorTab[parseInt(i)], "#000", 1, false)
            )
            if(c == 8){
                windowY += 18
                wX = 962
                c = 0
            }
            wX += 16
            c++
        }
        return form
    }

    form16() {
        const form = new Building(1012, 0, 95, 430, "#DCDCDC", "#000", 2, true,
            [],
            [])

        // For Window - Tab
        let window1Y =  window.innerHeight - this.height - 510
        let w1X = 1012
        let c = 1
        for(let j = 0; j < 21 ; j++){
            form.windowTab.push(
                new Window(w1X, window1Y, 31, 28,  "transparent", "#000", 1, false)
            )
            if(c == 3){
                window1Y += 28
                w1X = 981
                c = 0
            }
            w1X += 31
            c++
        }

        // For Window - All
        const windowsColorTab = ["red","white","#1B1A1A","DCDCDC"];
        let window2Y =  window.innerHeight - this.height - 499
        let wX = 1024
        let co = 1
        for(let j = 0; j < 18 ; j++){
            let i = Math.random() * windowsColorTab.length;
            form.windowTab.push(
                new Window(wX, window2Y, 8, 8,  windowsColorTab[parseInt(i)], "#000", 1, false)
            )
            if(co == 3){
                window2Y += 28
                wX = 993
                co = 0
            }
            wX += 31
            co++
        }

        return form
    }

    form17() {
        const form = new Building(1070, 0, 190, 100, "#878787", "#000", 1, true,
            [new Door(1090, 0, 36, 32, "#000", "#DCDCDC", 1, true),
                new Door(1150, 0, 85, 32, "#DCDCDC", "#000", 2, true)],
            [])

        // For Window - Tab
        const windowsColorTab = ["#2F2F2F","#DCDCDC","#878787"];
        let windowY =  window.innerHeight - this.height - 165
        let wX = 1102
        let c = 1
        for(let j = 0; j < 8 ; j++){
            let i = Math.random() * windowsColorTab.length;
            form.windowTab.push(
                new Window(wX, windowY, 31, 15,  windowsColorTab[parseInt(i)], "#000", 1, false)
            )
            if(c == 4){
                windowY += 21
                wX = 1071
                c = 0
            }
            wX += 31
            c++
        }
        return form
    }

    form18() {
        const form = new Building(1108, 0, 26, 145, "#FFF", "#000", 3, true)
        return form
    }

    form19() {
        const form = new Building(1135, 0, 215, 415, "#1B1A1A", "#000", 2, true,
            [],
            [])

        // For Window - All
        const windowsColorTab = ["#DCDCDC","violet"];
        let windowY =  window.innerHeight - this.height - 488
        let wX = 1142
        let c = 1
        for(let j = 0; j < 226 ; j++){
            let i = Math.random() * windowsColorTab.length;
            form.windowTab.push(
                new Window(wX, windowY, 10, 10,  windowsColorTab[parseInt(i)], "#000", 1, false)
            )
            if(c == 13){
                windowY += 18
                wX = 1126
                c = 0
            }
            wX += 16
            c++
        }
        return form
    }

    form20() {
        const form = new Building(1215, 0, 115, 200, "#878787", "#000", 1, true,
            [new Door(1242, 0, 36, 20, "#FFF", "#000", 2, true),
                new Door(1292, 0, 21, 20, "#FFF", "#000", 2, true)],
            [])

        // For Window - All
        const windowsColorTab = ["yellow","red","green"];
        let i = Math.random() * windowsColorTab.length;
        let windowY =  window.innerHeight - this.height - 268
        let wX = 1226
        let c = 1
        for(let j = 0; j < 9 ; j++){
            form.windowTab.push(
                new Window(wX, windowY, 92, 10,  windowsColorTab[parseInt(i)], "#000", 1, false)
            )
            if(c == 1){
                windowY += 18
                wX = 1210
                c = 0
            }
            wX += 16
            c++
        }
        return form
    }

    form21() {
        const form = new Building(1330, 0, 140, 375, "#585858", "#000", 2, true,
            [],[])

        // For Window - All
        let windowY =  window.innerHeight - this.height - 447
        let wX = 1340
        let c = 1
        for(let j = 0; j < 49 ; j++){
            form.windowTab.push(
                new Window(wX, windowY, 8, 32,  "white", "#000", 1, false)
            )
            if(c == 8){
                windowY += 43
                wX = 1324
                c = 0
            }
            wX += 16
            c++
        }
        return form
    }

    form22() {
        const form = new Building(1330, 0, 60, 100, "#878787", "#000", 2, true,
            [new Door(1348, 0, 25, 23, "#FFF", "#000", 2, true),
                new Door(1357, 0, 15, 23, "#000", "#000", 2, true)],
            [])

        // For Window - All
        const windowsColorTab = ["black","white"];
        let windowY =  window.innerHeight - this.height - 168
        let wX = 1340
        let c = 1
        for(let j = 0; j < 12 ; j++){
            let i = Math.random() * windowsColorTab.length;
            form.windowTab.push(
                new Window(wX, windowY, 8, 8,  windowsColorTab[parseInt(i)], "#000", 1, false)
            )
            if(c == 3){
                windowY += 15
                wX = 1324
                c = 0
            }
            wX += 16
            c++
        }
        return form
    }

    form23() {
        const form = new Building(1351, 0, 145, 300, "#C7C7C7", "#000", 1, true,
            [new Door(1413, 0, 30, 40, "#000", "#000", 2, true),
                new Door(1431, 0, 10, 40, "#585858", "#000", 1, true)],
            [])

        // For Window - All
        const windowsColorTab = ["aqua","transparent"];
        let windowY =  window.innerHeight - this.height - 365
        let wX = 1359
        let c = 1
        for(let j = 0; j < 104 ; j++){
            let i = Math.random() * windowsColorTab.length;
            form.windowTab.push(
                new Window(wX, windowY, 10, 10,  windowsColorTab[parseInt(i)], "#000", 1, false)
            )
            if(c == 8){
                windowY += 18
                wX = 1342
                c = 0
            }
            wX += 17
            c++
        }
        return form
    }

    form24() {
        let window1Y =  window.innerHeight - this.height - 158
        const form = new Building(1450, 0, 95, 90, "#DCDCDC", "#000", 2, true,
            [new Door(1467, 0, 60, 50, "#000", "#000", 2, true),
                new Door(1485, 0, 25, 15, "yellow", "#000", 1, true)],
            [new Window(1459, window1Y, 74, 15,  "red", "#000", 1, false)])

        // For Window - All
        let window2Y =  window.innerHeight - this.height - 118
        let wX = 1475
        let c = 1
        for(let j = 0; j < 3 ; j++){
            form.windowTab.push(
                new Window(wX, window2Y, 10, 10,  "yellow", "#000", 1, false)
            )
            wX += 17
            c++
        }
        return form
    }

    form25() {
        const form = new Building(1497, 0, 125, 235, "#DCDCDC", "#000", 2, true,
            [new Door(1515, 0, 70, 97, "#2C2C2C", "#000", 2, true)],
        [])

        // For Window - All
        const windowsColorTab = ["#1B1A1A","yellow","#DCDCDC","white"];
        let windowY =  window.innerHeight - this.height - 305
        let wX = 1500
        let c = 1
        for(let j = 0; j < 31 ; j++){
            let i = Math.random() * windowsColorTab.length;
            form.windowTab.push(
                new Window(wX, windowY, 8, 20,  windowsColorTab[parseInt(i)], "#000", 1, false)
            )
            if(c == 8){
                windowY += 32
                wX = 1483
                c = 0
            }
            wX += 17
            c++
        }
        return form
    }

    form26() {
        const form = new Building(1545, 0, 200, 50, "#878787", "#000", 2, true,
            [new Door(1575, 0, 55, 20, "#000", "#000", 2, true)],
            [])

        // For Window - All
        let windowY =  window.innerHeight - this.height - 118
        let wX = 1565
        let c = 1
        for(let j = 0; j < 10 ; j++){
            form.windowTab.push(
                new Window(wX, windowY, 8, 8,  "#FFF", "#000", 2, false)
            )
            wX += 17
            c++
        }
        return form
    }

    form27() {
        // For Window - 1
        const window1Y =  window.innerHeight - this.height - 565
        const window1ColorTab = ["navy","lime", "indigo"];
        let i = Math.random() * window1ColorTab.length;

        // For Window - 2
        const window2Y =  window.innerHeight - this.height - 565
        const window2ColorTab = ["gold","magenta", "pink"];
        let j = Math.random() * window2ColorTab.length;

        // For Window - 3
        const window3Y =  window.innerHeight - this.height - 565
        const window3ColorTab = ["red","aqua", "maroon"];
        let k = Math.random() * window3ColorTab.length;

        const form = new Building(1615, 0, 100, 490, "#000", "#000", 2, true,
            [new Door(1680, 0, 33, 131, "#878787", "#000", 2, true),
                new Door(1688, 0, 17, 122, "transparent", "#000", 3, true),
                new Door(1695, 0, 3, 110, "#000", "#000", 1, true)],
                [new Window(1621, window1Y, 18, 395, window1ColorTab[parseInt(i)], "#fff", 4, false),
                    new Window(1655, window2Y, 18, 395, window2ColorTab[parseInt(j)], "#fff", 4, false),
                    new Window(1690, window3Y, 18, 348, window3ColorTab[parseInt(k)], "#fff", 4, false)])
        return form
    }

    form28() {
        let window2Y =  window.innerHeight - this.height - 150
        const form = new Building(1615, 0, 165, 85, "#2F2F2F", "#000", 2, true,
            [new Door(1695, 0, 55, 50, "transparent", "#000", 2, true)],
            [new Window(1630, window2Y, 130, 8,  "#000", "#000", 2, false)])
        return form
    }

    /**
     * get array of forms
     * @return {[Building,...]}
     */
    static buildForms() {
        const bld = new Building()
        let forms = []
        forms.push(bld.form3(),
            bld.form5(),
            bld.form4(),
            bld.form2(),
            bld.form1(),
            bld.form6(),
            bld.form7(),
            bld.form8(),
            bld.form9(),
            bld.form11(),
            bld.form10(),
            bld.form12(),
            bld.form13(),
            bld.form14(),
            bld.form16(),
            bld.form15(),
            bld.form18(),
            bld.form19(),
            bld.form20(),
            bld.form17(),
            bld.form21(),
            bld.form23(),
            bld.form22(),
            bld.form25(),
            bld.form24(),
            bld.form27(),
            bld.form28(),
            bld.form26()
        )
        const builds = forms
        return builds
    }
}
export {Building}
